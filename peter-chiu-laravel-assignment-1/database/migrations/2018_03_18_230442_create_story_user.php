<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoryUser extends Migration
{
    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        //
        Schema::create('story_user', function (Blueprint $table) {
            $table->unsignedInteger('story_id');
            $table->unsignedInteger('user_id');
            // here we specify the compound primary key (it means the combination of the two keys must be unique)
            $table->primary([
                'story_id',
                'user_id'
            ]);
            $table->timestamps();
        });
    }

    /**
    * Reverse the migrations.
    *
    * @return void
    */
    public function down()
    {
        Schema::dropIfExists('story_user');
    }
}
