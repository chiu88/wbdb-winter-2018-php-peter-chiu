<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Faker\Factory;
use App\Models\Story;
use App\User;
use App\Models\Comment;
use App\Models\Contact;


Route::get('/contact', 'ContactController@create');
Route::post('/contact', 'ContactController@store');

Route::get('/story', 'StoryController@create');
Route::post('/story', 'StoryController@store');

Route::get('/story/{id}', 'StoryController@userStories');

Route::get('/stories/{id}/like/toggle', 'StoryController@toggleLike');

Route::get('/', 'MainController@load');
Route::post('/', 'MainController@store');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
