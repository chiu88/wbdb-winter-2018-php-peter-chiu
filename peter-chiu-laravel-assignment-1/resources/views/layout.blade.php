<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="/css/app.css">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <title>Google News</title>

    </head>


    <div class="header-container">
        @if (Auth::check())
            Hello, {{ Auth::user()->name }}
            <a href="{{ route('logout') }}"
                onclick="event.preventDefault();
                         document.getElementById('logout-form').submit();">
                Logout
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        @else

            <a href="/login">Login</a>
            <a href="/register">Register</a>
        @endif

        <header class="header flex flex-horizontal">
            <div class="hamburger">
                <i class="fa fa-bars fa-lg icon"></i>
                <img src="./img/google-news-icon.png">
            </div>
            <div class="search-bar">
                <i class="fa fa-search fa-lg icon"></i>
                <input type="text" name="" placeholder="Search">
            </div>
            <div class="account-icons">
                <i class="fa fa-angellist fa-lg"></i>
                <i class="fa fa-bell fa-lg"></i>
                <i class="fa fa-user-circle fa-2x"></i>
            </div>
        </header>

        <div class="navigation-links flex flex-horizontal">
            <ul class='flex flex-horizontal'>

                <li><a href="/">Home</a></li>
                <li><a href="/story">Post a News Story</a></li>
                <li><a href="/contact">Contact Us</a></li>
            </ul>
        </div>
    </div>

    <?php if($errors->any()):    ?>
        <div class="alert alert-danger">
            <ul>
                <?php foreach ($errors->all() as $error): ?>
                    <li>{{$error}}</li>
                <?php endforeach; ?>
            </ul>
        </div>
    <?php endif; ?>
    <body>

        @yield('content')
    </body>
</html>
