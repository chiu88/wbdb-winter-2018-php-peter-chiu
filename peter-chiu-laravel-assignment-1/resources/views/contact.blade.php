@extends('layout')

@section('content')

<!-- This will show that the data was input successfully and stored -->
<?php if($message = session('message')): ?>
    <div class="alert alert-success">
        {{$message}}
    </div>
<?php endif; ?>




<div class="body-section">
  <h1>Contact Form</h1>
  <form method="post">
    {{csrf_field()}}

    @include('forms.text', [
    'name' => 'firstName',
    'label' => 'First Name:'
    ])

    @include('forms.text', [
    'name' => 'lastName',
    'label' => 'Last Name:'
    ])

    @include('forms.text', [
    'name' => 'phoneNumber',
    'label' => 'Phone Number:'
    ])

    @include('forms.text', [
    'name' => 'email',
    'label' => 'Email:'
    ])


    <input type="submit" name="" value="Submit">

  </form>

</div>

@endsection
