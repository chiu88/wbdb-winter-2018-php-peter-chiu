@extends('layout')

@section('content')
<div class="body-section">

  <h1>News Feeds posted by: {{ $user->name }}</h1>

  <ul>
    @foreach ($stories as $story)
    <li>{{ $story->content }}</li>
    <br>
    @endforeach


  </ul>
</div>
@endsection
