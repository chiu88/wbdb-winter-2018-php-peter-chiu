@extends('layout')

@section('content')


<div class="body-section">

  <h1>Post a news article</h1>
  <form method="post">
    {{csrf_field()}}
    Write your post here: <br>
    <textarea name="story" rows="8" cols="80"></textarea>
    <input type="submit" name="" value="Submit">
  </form>
</div>
@endsection
