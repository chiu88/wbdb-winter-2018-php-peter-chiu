@extends('layout')

@section('content')

<div class="body-section flex flex-horizontal">

  <div class="section flex flex-vertical">

    <p>Sections</p>
    <a href=""><i class="fa fa-globe fa-lg world"></i>Top Stories</a>
    <a href=""><i class="fa fa-globe fa-lg world"></i>World</a>
    <a href=""><i class="fa fa-globe fa-lg world"></i>Canada</a>
    <a href=""><i class="fa fa-globe fa-lg world"></i>Busines</a>
    <a href=""><i class="fa fa-globe fa-lg world"></i>Technology</a>
    <a href=""><i class="fa fa-film fa-lg film"></i>Entertainment</a>
    <a href=""><i class="fa fa-bicycle fa-lg bike"></i>Sport</a>
    <a href=""><i class="fa fa-globe fa-lg world"></i>Science</a>
    <a href=""><i class="fa fa-globe fa-lg world"></i>Health</a>
    <a href="">Manage sections</a>

  </div>



  <div class="story-section-container">

    <div class="topStories-section spacing">


      <ul>
        <h3>Top Stories</h3>
        <?php foreach($stories as $story): ?>
          <li>

            <ul class="story">
              <div class="story-info-container flex flex-horizontal">

                <div class="image-container">
                  <a href="./story/{{$story->user->id}}"><img class='images' src="{{$story->user->image}}"></a>
                </div>
                <div class="story-info">
                  <div class="small-faded">

                    Article posted by: <strong><a href="./story/{{$story->user->id}}">{{$story->user->name}} @ {{$story->user->handle}}</strong></a>
                  </div>
                  <div class="title">
                    {{$story->content}}
                  </div>
                  <br>
                  Liked by {{count($story->likes)}} users.
                  <br>
                  @if ($story->isLikedByCurrentUser())
                      <a href="/stories/{{ $story->id }}/like/toggle">unlike</a>
                  @else
                      <a href="/stories/{{ $story->id }}/like/toggle">like</a>
                  @endif
                  <br>
                  Comments: <br>
                  <form method="post">
                    {{csrf_field()}}
                    <input type="text" name="storyComment" placeholder="Tell us what you think">
                    <input type="hidden" name="storyId" value="{{$story->id}}">
                    <input type="submit" name="" value="Submit">
                  </form>
                  <br>
                  @foreach($story->comments as $comment)
                    <strong>{{$comment->user->name}}</strong> @ {{$comment->user->handle}} says: <br>
                    {{$comment->content}}<br>

                  @endforeach

                </div>
              </div>


            </ul>
          </li>

        <?php endforeach; ?>

      </ul>
    </div>


  </div>

</div>



@endsection
