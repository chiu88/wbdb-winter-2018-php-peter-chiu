<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Faker\Factory;

use App\User;
use App\Models\Story;
use App\Models\Comment;
use App\Models\Contact;

class MainController extends Controller
{
    //This is where we populate the database
    function load() {
        $faker = Factory::create();

        $users = User::all();

        $stories = Story::all();

        $comments = Comment::all();



        $data = [
            'users' => $users,
            'stories' => $stories,
            'comments' => $comments,
        ];

        return view('welcome', $data);
    }

    function store() {
      if (!\Auth::check()) {
          return redirect('/')->with('message', 'Please sign in to post a comment');
      }

        // user validation
        $request = request();
        $result = $request->validate(
            ['storyComment' => 'required|max:150'],
            ['storyComment.max' => 'Please enter a comment smaller than 150 characters ']
        );

        // the function store will post the data into the database, in our route file, we have specified that this function runs in the post route
        $data = request()->all();
        $loggedInUser = $request->user();

        $comment = new Comment();

        $comment->story_id = $data['storyId'];
        $comment->user_id = $loggedInUser->id;
        $comment->content = $data['storyComment'];
        $comment->save();

        return redirect('/')->with('message', 'Your comment was successfully posted!');
    }

}
