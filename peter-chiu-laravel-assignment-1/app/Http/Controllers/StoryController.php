<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Models\Story;
use App\Models\Comment;
use App\Models\Contact;

class StoryController extends Controller
{
    //
    function create() {

        if (!\Auth::check()) {
            return redirect('/login');
        }

        return view('story');
    }

    function store() {
        if (!\Auth::check()) {
            return redirect('/login');
        }

        // user validation
        $request = request();
        $result = $request->validate(
            ['story' => 'required|max:255'],

            ['story.max' => 'Please enter a news feed smaller than 255 characters']
        );

        $loggedInUser = $request->user();

        // the function store will post the data into the database, in our route file, we have specified that this function runs in the post route

        $data = request()->all();

        $story = new Story();
        $story->user_id = $loggedInUser->id;
        $story->content = $data['story'];
        $story->save();



        return redirect('/')->with('message', 'Your story was successfully posted!');
    }

    function userStories($userId)
    {
        $user = User::where('handle', $userId)
            ->orWhere('id', $userId)
            ->first();

        if (!$user) {
            abort(404);
        }

        $stories = $user->stories;

        return view('userStories', [
            'user' => $user,
            'stories' => $stories
        ]);
    }

    public function toggleLike($storyId)
    {
        $user = request()->user();
        $story = Story::find($storyId);

        if ($story->isLikedByCurrentUser()) {
            $story->likes()->detach($user);
        } else {
            $story->likes()->attach($user);
        }

        return back()
            ->with('message', 'You successfully liked a story.');
    }
}
