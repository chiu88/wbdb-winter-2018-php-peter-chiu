<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Models\Story;

class Comment extends Model
{
    public function stories() {
        return $this->belongsTo(Story::class);
    }

    public function user() {
        return $this->belongsTo(User::class);
    }
}
